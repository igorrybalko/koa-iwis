const Router = require('koa-router');
const router = new Router({ prefix: '/user' });

router.get('/', (ctx, next) => {
    ctx.status = 200;
    ctx.body = {"some": 1, "key": "dddd"};
});

module.exports = router;
