const combineRouters = require('koa-combine-routers');


const router = combineRouters(
    require('./user')
);

module.exports = router;
