const Koa = require('koa');
require('dotenv').config();
const bodyParser = require('koa-bodyparser');

const app = new Koa();

app.use(require('./routers')());

app.use(bodyParser());

app.listen(3003);
